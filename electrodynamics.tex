\documentclass[11pt,a4paper,english]{article}
% Alle packages en dergelijke
\usepackage{amsmath,amssymb,array,babel,geometry,caption,siunitx,esint,commath}
\usepackage[usenames,dvipsnames]{xcolor}

\sisetup{inter-unit-product=\ensuremath{{}\cdot{}}}

% Aangepaste titel in UT huisstijl
\usepackage[dutch,style,nomath]{utwentetitle}

\title{Mathematics}
\subtitle{for electrodynamics}
%\course{Electrodynamics}
\author{Silke Hofstra}
%\faculty{Faculteit Elektrotechniek, Wiskunde en Informatica}

\numberwithin{equation}{section}


% Macro’s
\newcommand*\eps[0]{\varepsilon_0}
\newcommand*\mun[0]{\mu_0}
\renewcommand*{\vec}[1]{\mathbf{#1}}

\renewcommand*{\od}[3][]{%
	\frac{\dif^{#1}#2}{\dif #3^{#1}}%
}
\renewcommand*{\pd}[3][]{%
	\frac{\partial^{#1}#2}{\partial #3^{#1}}%
}
\newcommand*{\grad}[1]{\nabla#1}
\newcommand*{\curl}[1]{\nabla\times#1}
\renewcommand*{\div}[1]{\nabla\cdot#1}
\newcommand*{\quabla}[1]{\nabla^2#1 - \frac{1}{c^2}\pd[2]{#1}{t}}

\newcommand*\feynref[1]{\emph{The Feynman Lectures on Physics} (II, p. #1)}

\begin{document}
\maketitle
This document is meant to be a somewhat comprehensive summary of the most important laws, equations and relations necessary for \emph{Electrodynamics}. Table 15--1 from \feynref{15--15} is also included in this summary on page~\pageref{tab:15}.

\tableofcontents\clearpage

\section{Notation}
\begin{itemize}
	\item A normal letter (eg: $a$) denotes a scalar.
	\item A capital letter (eg: $T$) denotes a scalar field.\\
		(eg: $A(x,y,z) = 56x+2y-6z$)
	\item A bold letter (eg: $\vec h$) denotes a vector.\\
		(eg: $\vec h=(2,6,-1)$)
	\item A capital and bolt letter (eg: $\vec{B}$) denotes either a vector (above) or a vector field.\\
		(eg: $\vec{A}(x,y,z) =\left(\sin x,\sin y,\sin z \right)$)
	\item A numbered parameter (eg: $f(1)$) means you have to fill in the position of the point with that number. For $f(x,y,z,t)$ this means $f(2,t) = f(x_2,y_2,z_2,t)$. See \feynref{21--4}.
\end{itemize}


\section{Symbols and units}
\begin{table}[h]
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{|c|c|l|}
	\hline
	Symbol & Value/Unit & Description \\ \hline
	$\vec{B}$     & \si{T} & magnetic field \\ \hline
	$\vec{E}$     & \si{V.m^{-1}} or \si{N.C^{-1}} & electric field \\ \hline
	$\vec{j}$     & \si{A.m^{-2}} & current density \\ \hline
	$Q$ or $q$    & \si{C} & charge \\ \hline
	$\rho$        & \si{C.m^{-3}} & (volume) charge density \\ \hline
	$\sigma$      & \si{C.m^{-2}} & surface charge density \\ \hline
	$\lambda$     & \si{C.m^{-1}} & linear charge density \\ \hline
	$\mu_0$       & $ \SI{4\pi e-7}{H.m^{-1}} $ & magnetic permeability \\ \hline
	$\eps$  & $\frac{1}{\mu_0 c^2} \approx \SI{8,8542e12}{F.m^{-1}}$ & permittivity of free space \\ \hline
	%$\frac1{4\pi\eps}$ & \SI{e-7}{{\clight^2}.N.m^2.C^{-2}} or \SI{9.0 e9}{V.m.C^{-1}} & \\ \hline
	$ \mathcal{L} $ & & \\ \hline
	$ \mathcal{V} $ & & \\ \hline
	$ \vec{A} $     & \si{V.s.m^{-1}} & vector potential \\ \hline
	$ \phi $  & \si{V} & scalar potential \\ \hline
\end{tabular}
\end{table}
\section{Basics}
\subsection{Vector operations}
\begin{align*}
	\vec{A} \cdot \vec{B} &= A_xB_x+A_yB_y+A_zB_z &= \text{scalar}\\
	\vec{A} \times \vec{B} &= \begin{pmatrix} A_yB_z - A_zB_y \\ A_zB_x - A_xB_z \\ A_xB_y - A_yB_x\end{pmatrix} &= \text{vector}\\
	\vec{A} \times \vec{A} &= 0 \\
	\vec{A} \cdot (\vec{A} \times \vec{B}) &= 0 \\
	\vec{A} \cdot (\vec{B} \times \vec{C}) &= (\vec{A} \times \vec{B}) \cdot \vec{C}\\
	\vec{A} \times (\vec{B} \times \vec{C}) &= \vec{B}(\vec{A} \cdot \vec{C}) - \vec{C}(\vec{A} \cdot \vec{B})\\
\end{align*}

\subsection{Nabla operations}
\begin{align*}
	\nabla &= \left( \pd{}{x}, \pd{}{y}, \pd{}{z} \right) \\
	\grad{T} &= \text{grad }T &= \text{vector}\\
	\div{\vec h} &= \text{div }\vec h &= \text{scalar}\\
	\curl{\vec h} &= \text{curl }\vec h &= \text{vector}\\
	\div{(\grad{T})} &= \nabla^2T &= \text{scalar field}\\
	\curl{(\grad{T})} &= 0\\
	\grad{(\div{\vec h})} &= \hdots &=\text{vector field}\\
	\div{(\curl{\vec h})} &=0\\
	\curl{(\curl{\vec h})} &= \grad{(\div{\vec h})} - \nabla^2\vec h &= \text{vector field $-$ scalar field}\\
	(\div{\nabla})\vec h &= \nabla^2\vec h &= \text{vector field}
\end{align*}

\subsection{Integrals}
\begin{align*}
	\psi(1)-\psi(2) &=
		\underset{\text{along curve}}{\int_{(1)}^{(2)}} \nabla\psi \cdot \dif\vec{s} &
		\text{(line integral)} \\
\end{align*}

\section{Electrostatics}

\subsection{Gauss' Law}
\begin{align*}
	\int\limits_{\substack{\text{any closed} \\ \text{surface }S}}
		E_n \dif a &=
		\frac{\text{sum of charges inside}}{\eps} \\
	%
	\int\limits_{\substack{\text{any closed} \\ \text{surface }S}}
		\vec E \cdot \vec n \dif a &=
		\frac{Q_{int}}{\eps} \\
	%
	Q_{int} & = \sum \limits_{\text{inside }S} q_i \\
	        & = \int\limits_{\substack{\text{volume} \\ \text{inside }S}} \rho \dif V \\
	%
	\implies \curl{\vec E} &= \frac{\rho}{\eps}
\end{align*}

\subsection{Other}
\begin{itemize}
	\item The difference of the values of a scalar field at two points is equal to the line integral of the tangential component of the gradient of that scalar along any curve at all between the first and second points:\[ \psi(2)-\psi(1) = \underset{\text{any curve}}{\int_{(1)}^{(2)}} \nabla\psi \cdot d\vec{s} \]
	\item The surface integral of the normal component of an arbitrary vector over a closed surface is equal to the integral of the divergence of the vector over the volume interior to the surface: \[\int\limits_{\substack{\text{closed} \\ \text{surface}}} \vec{C \cdot n}~ da = \int\limits_{\substack{\text{volume} \\ \text{inside}}} \nabla \cdot \vec{C} ~ dV\]
	\item The line integral of the tangential component of an arbitrary vector over a closed surface is equal to the integral of the divergence of the vector over the volume interior to the surface: \[\int\limits_{\text{boundary}} \vec{C} \cdot d\vec{s} = \int\limits_{\text{surface}} (\nabla \times \vec{C})\cdot \vec{n} ~ da\]
\end{itemize}

\subsection{Coulomb's Law}
\begin{align*}
	\vec{F_1} &=
		\frac1{4\pi\eps}\frac{q_1q_2}{r_{1 2}^2}\vec e_{1 2} =
		-\vec{F_2} \\
	\implies \vec E(1) &=
		\frac1{4\pi\eps}\frac{q_2}{r_{1 2}^2}\vec e_{1 2} \\
	\vec{E} &=
		\frac1{4\pi\eps}\int\limits_{\substack{\text{all} \\ \text{space}}}
		\frac{\rho(2)\vec e_{1 2} \dif V_2}{r_{1 2}^2}
\end{align*}

\subsection{Electric Potential}
\begin{align*}
	\phi(1) &= \frac1{4\pi\eps}\int\frac{\rho(2) \dif V_2}{r_{1 2}}\\
	 -\nabla\phi &= \vec{E}\\
	\curl{\vec E} &= 0\\
	\oint\vec E\cdot \dif\vec s &= 0\\
\end{align*}

\clearpage
\section{Relativity}

\subsection{Lorentz transform in $x$-direction}
\def\gamm{\frac{1}{\sqrt{1-\frac{v^2}{c^2}}}}
\[  \]
\begin{align*}
	x' &= \gamma\left(x - vt\right) &              x &= \gamma\left(x' - vt' \right) \\
	y' &= y &                                      y &= y'\\
	z' &= z &                                      z &= z'\\
	t' &= \gamma\left(t - \frac{vx}{c^2} \right) & t &= \gamma\left(t' + \frac{vx'}{c^2} \right) \\
	\gamma &= \gamm &                              s &= \frac{v+u}{1+\frac{v u}{c^2}}
\end{align*}

\subsection{Mass/energy relation}
\[ E^2 = \left(pc \right)^2 + \left(m_0 c^2 \right)^2 \]

With some assumptions, this results in the following relations:

\begin{align*}
	p &= E \frac{v}{c^2} & p &= \gamma mv \\
	E_0 &= m c^2           & E &= \gamma E_0 = \gamma m c^2 \\
	m_0 &= \frac{E_0}{c^2} & m_{rel} &= \frac{E}{c^2} = \gamma m_0 \\
\end{align*}

\clearpage
\section{Electrodynamics}

\subsection{Maxwell’s equations}

\def\maxwelldivE{\nabla \cdot \vec{E} = \frac{\rho}{\eps}}
\def\maxwelldivB{\nabla \cdot \vec{B} = 0}
\def\maxwellcurlE{\nabla \times \vec{E} = -\pd{\vec{B}}{t}}
\def\maxwellcurlB{\nabla \times \vec{B} = \mu_0 \left(\vec{J}
	+ \eps \pd{\vec{E}}{t} \right)}

\def\maxwelliintE{%
	\oiint_{\partial\Omega} \vec{E} \cdot \dif\vec{S}
		= \frac{1}{\eps} \iiint_\Omega\rho \dif V}
\def\maxwelliintB{
	\oiint_{\partial\Omega} \vec{B} \cdot \dif\vec{S}
		= 0}
\def\maxwellintE{
	\oint_{\partial\Sigma} \vec{E} \cdot \dif\vec\ell 
		= -\od{}{t} \iint_{\Sigma} \vec{B} \cdot \dif\vec{S}}
\def\maxwellintB{
	\oint_{\partial\Sigma} \vec{B} \cdot \dif\vec\ell 
		= \mu_0 \iint_{\Sigma} \vec{J}\cdot\dif\vec{S} 
		+ \mu_0\eps\od{}{t}\iint_{\Sigma} \vec{E}\cdot\dif\vec{E}}

\begin{table}[h]
\everymath{\displaystyle}
\renewcommand{\arraystretch}{2}
\begin{tabular}{|l|l|l|}\hline
	Name & Differential & Integral \\ \hline
	Gauss &
		$\maxwelldivE$ &
		$\maxwelliintE$ \\
	Gauss &
		$\maxwelldivB$ &
		$\maxwelliintB$ \\
	Faraday &
		$\maxwellcurlE$ &
		$\maxwellintE$ \\
	Ampère &
		$\maxwellcurlB$ &
		$\maxwellintB$ \\ \hline
\end{tabular}
\end{table}

\subsection{Vector and scalar potential}
\begin{align*}
	\curl{\vec{A}} &= \vec{B} \\
	\div{\vec{A}} &= -\frac{1}{c^2} \pd{\phi}{t} \\
	\vec{E} &= -\nabla\phi -\pd{\vec{A}}{t} 
\end{align*}

\subsection{Wave equation}
\[ \Box f = \quabla{f} = 0\]

\subsection{Waveguides}
\begin{align*}
	\omega &= c k              & \text{(dispersion relation)} \\
	\omega_c &= c\frac{\pi}{a} & \text{(cut-off frequency)} \\
	v_{g} &= \pd{\omega}{k}    & \text{(group velocity)} \\
	v_{p} &= \frac{\omega}{k}  & \text{(phase velocity)} \\
\end{align*}

General situation:
\begin{align*}
	E_y &= E_0 \sin\left(k_x x \right) e^{i \left(\omega t - k_z z \right)} \\
	k_x &= \frac{\pi}{a} \\
	\omega_c &= c k_x = c\frac{\pi}{a} \\
	k_z &= \sqrt{\frac{\omega^2}{c^2} - k_x^2} =
		\frac{1}{c} \sqrt{\omega^2 - \omega_c^2} \\
	v_{g} &= \pd{\omega}{k} = c\sqrt{1-\frac{\omega_c^2}{\omega^2} } \\
	v_{p} &= \frac{\omega}{k_z} = \frac{c}{\sqrt{1-\frac{\omega_c^2}{\omega^2}}}
\end{align*}

\subsection{Poynting vector}
\[ \vec S = \frac{1}{\mu_0} \vec E \times \vec B \]



% Feynman table
\clearpage
\begin{table}[p]
	% Table commands etc
	\newcommand\cellcenter[1]{\multicolumn{1}{|c|}{\bfseries #1}}
	\newlength\maxarrlength
	\newcommand\maxarr[0]{\hspace{-\maxarrlength}\boldsymbol\Rightarrow}
	\settowidth{\maxarrlength}{$\maxarr~$}
	\newlength\twl
	\settowidth{\twl}{and~}
	\newcommand\midword[0]{\hspace{-\twl}}
	\everymath{\displaystyle}
	\renewcommand{\arraystretch}{1.78}
	% Captions/labels
	\label{tab:15}
	\caption*{\bfseries Table 15--1}
	% Centered table
	\centerline{%
	\begin{tabular}{| >{\hspace{8mm}}p{.6\textwidth} | >{\hspace{8mm}}p{.6\textwidth} |} 
	\hline
		\cellcenter{FALSE IN GENERAL (true only for statics)} &
			\cellcenter{TRUE ALWAYS} \\ \hline
		$F = \frac{1}{4\pi\eps} \frac{q_1q_2}{r^2}$ \hfill (Coulomb's law) &
			$\vec{F} = q(\vec{E} + v \times \vec{B})$ \hfill (Lorentz force) \\
		& %empty
			$\maxarr \div{\vec E} = \frac{\rho}{\eps}$ \hfill (Gauss' Law) \\[2mm] \hline
		$\curl{\vec E} = 0$ &
			$ \maxarr \curl{\vec E} = -\pd{\vec B}{t}$ \hfill (Faraday's law)\\
		$\vec{E} = -\nabla \phi$ &
			$ \vec{E} = -\nabla \phi - \pd{\vec A}{t}$\\
		$\vec{E}(1) = \frac1{4\pi\eps}\int\frac{\rho(2)\vec e_{12} \dif V_2}{r_{12}^2}$ &
			 \\ %empty
		For conductors, $\vec{E} = 0$, $\phi = $ constant. $Q = CV$ &
			In a conductor, $\vec{E}$ makes currents\\ \hline
		& %empty
			$\maxarr \div{\vec B} = 0 $ \hfill (No magnetic charges) \\
		& %empty
			$\vec{B} = \curl{\vec A}$ \\
		$c^2\curl{\vec B} = \frac{\vec{j}}{\eps}$ \hfill (Ampères Law)&
			$\maxarr c^2\curl{\vec B} = \pd{\vec E}{t} + \frac{\vec{j}}{\eps}$ \\
		$\vec B(1) = \frac1{4\pi\eps}\int\frac{\rho(2)\vec e_{12}}{\vec r_{12}}\dif V_2$ &
			\\[2mm] \hline % empty
		& \\[-8mm] % extra spacing
		$\nabla^2\phi=-\frac{\rho}{\eps}$ \hfill (Poisson's equation) &
			$\nabla^2 \phi - \frac{1}{c^2} \pd[2]{\phi}{t} = - \frac{\rho}{\eps}$\\[-5pt]
		& \midword and \\[-5pt]
		$\nabla^2\vec{A} = -\frac{\vec{j}}{\eps c^2}$ &
			$\displaystyle \nabla^2 A - \frac{1}{c^2} \pd[2]{\vec A}{t} = -\frac{\vec j}{\eps c^2}$ \\[-5pt]
		\midword with & \midword with \\[-5pt]
		$ \div{\vec A} = 0$ &
			$c^2 \div{\vec A} + \pd{\phi}{t} = 0$\\[2mm] \hline & \\[-8mm]
		$\phi(1) = \frac1{4\pi\eps} \int \frac{\rho(2)}{r_{12}} \dif V_2$ &
			$\phi(1,t) = \frac1{4\pi\eps} \int \frac{\rho(2,t')}{r_{12}} \dif V_2$\\[-5pt]
		& \midword and \\[-5pt]
		$\vec A(1) = \frac1{4\pi\eps c^2} \int \frac{\vec j(2)}{r_{12}}\dif V_2$ &
			$\vec A(1,t)\pm\frac1{4\pi\eps c^2}\int\frac{\vec j(2,t')}{r_{12}}\dif V_2 $\\[-5pt]
		& \midword with \\[-5pt]
		&
			$ t' = t-\frac{r_{12}}c$ \\[2mm] \hline & \\[-8mm]
		$ U = \tfrac12 \int \rho \phi \dif V+\tfrac12\int \vec j \cdot \vec A \dif V$ &
			$U=\int \left(\frac{\eps}2 \vec E \cdot \vec E + \frac{\eps c^2}2 \vec B \cdot \vec B \right) \dif V$ \\[2mm]
		\hline
	\end{tabular}}
	\flushleft
	The equations marked by an arrow ($\hspace{\maxarrlength}\maxarr$) are Maxwell's equations.
\end{table}

\end{document}